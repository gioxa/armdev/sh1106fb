# Patch Gioxa Ltd for SH1106 oLEd framebuffer driver and support col offset for ssd1307fb
# Danny Goossen<danny.goossen@gioxa.com>
diff --git a/Documentation/devicetree/bindings/display/sh1106fb.txt b/Documentation/devicetree/bindings/display/sh1106fb.txt
new file mode 100644
index 0000000..eb469b9
--- /dev/null
+++ b/Documentation/devicetree/bindings/display/sh1106fb.txt
@@ -0,0 +1,60 @@
+* Gioxa SH1106 Framebuffer Driver
+
+Required properties:
+  - compatible: Should be "gioxa,<chip>fb-<bus>". The only supported bus for
+    now is i2c, and the supported chips are sh1106.
+  - reg: Should contain address of the controller on the I2C bus. Most likely
+         0x3c or 0x3d
+
+Optional properties:
+  - gioxa,height: Height in pixel of the screen driven by the controller
+  - gioxa,width: Width in pixel of the screen driven by the controller
+  - vbat-supply: The supply for VBAT
+  - gioxa,segment-no-remap: Display needs normal (non-inverted) data column
+                              to segment mapping
+  - gioxa,com-seq: Display uses sequential COM pin configuration
+  - gioxa,com-lrremap: Display uses left-right COM pin remap
+  - gioxa,com-invdir: Display uses inverted COM pin scan direction
+  - gioxa,com-offset: Number of the COM pin wired to the first display line
+  - gioxa,col-offset: Number of the SEG pin wired to the first display colom
+  - gioxa,prechargep1: Length of deselect period (phase 1) in clock cycles.
+  - gioxa,prechargep2: Length of precharge period (phase 2) in clock cycles.
+                         This needs to be the higher, the higher the capacitance
+                         of the OLED's pixels is
+  - gioxa,dclk-div: Clock divisor 1 to 16
+  - gioxa,dclk-frq: Clock frequency 0 to 15, higher value means higher
+                      frequency
+  - gioxa,lookup-table: 8 bit value array of current drive pulse widths for
+                          BANK0, and colors A, B, and C. Each value in range
+                          of 31 to 63 for pulse widths of 32 to 64. Color D
+                          is always width 64.
+
+Defaults:
+  - gioxa,height: 64
+  - gioxa,width: 128
+  - vbat-supply: none
+  - gioxa,col-offset: 2
+  - gioxa,com-offset: 0
+  - gioxa,dclk-div: 1
+  - gioxa,dclk-frq: 8
+  - gioxa,prechargep1: 2
+  - gioxa,prechargep2: 2
+
+Set for basic on shelf modules:
+  - Set gioxa,com-invdir;
+  - Set gioxa,com-seq;
+
+To set if needed:
+
+  - gioxa,segment-no-remap
+  - gioxa,com-lrremap:
+
+Example:
+
+sh1106: oled@3c {
+        compatible = "gioxa,sh1106fb-i2c";
+        reg = <0x3c>;
+        gioxa,com-invdir;
+        gioxa,com-seq;
+        status = "ok";
+};
diff --git a/drivers/video/fbdev/Kconfig b/drivers/video/fbdev/Kconfig
index 1e70e83..33e4d5c 100644
--- a/drivers/video/fbdev/Kconfig
+++ b/drivers/video/fbdev/Kconfig
@@ -2249,6 +2249,22 @@ config FB_SSD1307
 	  This driver implements support for the Solomon SSD1307
 	  OLED controller over I2C.
 
+config FB_SH1106
+	tristate "Gioxa SH1106 framebuffer support"
+	default y
+	depends on FB && I2C
+	depends on OF
+	depends on GPIOLIB || COMPILE_TEST
+	select FB_SYS_FOPS
+	select FB_SYS_FILLRECT
+	select FB_SYS_COPYAREA
+	select FB_SYS_IMAGEBLIT
+	select FB_DEFERRED_IO
+	select FB_BACKLIGHT
+	help
+	  This driver implements support for the Gioxa SH1106
+	  OLED controller over I2C.
+
 config FB_SM712
 	tristate "Silicon Motion SM712 framebuffer support"
 	depends on FB && PCI
diff --git a/drivers/video/fbdev/Makefile b/drivers/video/fbdev/Makefile
index aa63527..8da911b 100644
--- a/drivers/video/fbdev/Makefile
+++ b/drivers/video/fbdev/Makefile
@@ -130,7 +130,7 @@ obj-$(CONFIG_FB_OF)               += offb.o
 obj-$(CONFIG_FB_MX3)		  += mx3fb.o
 obj-$(CONFIG_FB_DA8XX)		  += da8xx-fb.o
 obj-$(CONFIG_FB_SSD1307)	  += ssd1307fb.o
+obj-$(CONFIG_FB_SH1106)           +=gioxa/
 obj-$(CONFIG_FB_SIMPLE)           += simplefb.o
-
 # the test framebuffer is last
 obj-$(CONFIG_FB_VIRTUAL)          += vfb.o
diff --git a/drivers/video/fbdev/gioxa/Makefile b/drivers/video/fbdev/gioxa/Makefile
new file mode 100644
index 0000000..bd27c49
--- /dev/null
+++ b/drivers/video/fbdev/gioxa/Makefile
@@ -0,0 +1,7 @@
+# SPDX-License-Identifier: GPL-2.0
+#
+# Makefile for the GIOXA framebuffer driver
+#
+
+obj-$(CONFIG_FB_SH1106)  += sh1106fb.o gioxa_embedded_logo.o
+
diff --git a/drivers/video/fbdev/gioxa/gioxa_embedded_logo.c b/drivers/video/fbdev/gioxa/gioxa_embedded_logo.c
new file mode 100644
index 0000000..0800fce
--- /dev/null
+++ b/drivers/video/fbdev/gioxa/gioxa_embedded_logo.c
@@ -0,0 +1,100 @@
+#include <linux/module.h>
+#include <linux/kernel.h>
+#include <linux/string.h>
+
+#include "gioxa_embedded_logo.h"
+
+unsigned char gioxa_embedded_bits[] = {
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1E, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0xE0, 0x3F, 0x1E, 0xFC, 0xC1, 0x87, 0x0F, 0x7C, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0xF8, 0x7F, 0x1E, 0xFF, 0x87, 0x87, 0x07, 0x7E,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFC, 0x7F, 0x80, 0xFF,
+  0x87, 0xCF, 0x03, 0x7E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0xFE, 0x38, 0xCF, 0xCF, 0x0F, 0xEF, 0x01, 0x7F, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x3E, 0x00, 0xCF, 0x07, 0x0F, 0xFF, 0x80, 0x7F,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0x00, 0xEF, 0x03,
+  0x0F, 0x7E, 0x80, 0xF7, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x8F, 0x3F, 0xEF, 0x01, 0x0F, 0x3E, 0xC0, 0xF3, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0xCF, 0x1F, 0xE7, 0x01, 0x0F, 0x3E, 0xC0, 0xF3,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xCF, 0x9F, 0xE7, 0x81,
+  0x0F, 0x7F, 0xE0, 0xF1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x0F, 0x9E, 0xE7, 0xC1, 0x87, 0x7F, 0xE0, 0xFF, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x3F, 0x9F, 0xE7, 0xE3, 0xC7, 0xFB, 0xF0, 0xFF,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFE, 0x8F, 0xC7, 0xFF,
+  0xE3, 0xF3, 0xF8, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0xFE, 0xCF, 0xC3, 0xFF, 0xF1, 0xF1, 0x79, 0xF0, 0x01, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0xF8, 0xC7, 0x03, 0x7F, 0xF8, 0xE0, 0x3D, 0xE0,
+  0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80,
+  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00,
+  0x80, 0x01, 0x0C, 0x00, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x01,
+  0x00, 0x30, 0x00, 0x00, 0x80, 0x01, 0x0C, 0x00, 0x80, 0x01, 0x00, 0x00,
+  0x00, 0x00, 0x07, 0x00, 0x00, 0x30, 0x00, 0x00, 0x80, 0x01, 0x0C, 0x00,
+  0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x30, 0x00, 0x00,
+  0x80, 0x01, 0x0C, 0x00, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0x07, 0xF8,
+  0xFB, 0xF0, 0x07, 0x3E, 0xF8, 0xC1, 0x0F, 0x3E, 0xF8, 0x01, 0x00, 0x00,
+  0x00, 0x00, 0x07, 0x78, 0x9E, 0xF1, 0x0C, 0x73, 0x8C, 0x61, 0x0C, 0x73,
+  0x8C, 0x01, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x19, 0x86, 0x31, 0x9C, 0x61,
+  0x86, 0x31, 0x8C, 0x61, 0x86, 0x01, 0x00, 0x00, 0x00, 0x00, 0x07, 0x18,
+  0x86, 0x31, 0x98, 0xE1, 0x86, 0x31, 0x8C, 0xE1, 0x86, 0x01, 0x00, 0x00,
+  0x00, 0x00, 0x07, 0x18, 0x86, 0x31, 0x98, 0xFF, 0x86, 0x31, 0x8C, 0xFF,
+  0x86, 0x01, 0x00, 0x00, 0x00, 0x00, 0x07, 0x18, 0x86, 0x31, 0x98, 0x01,
+  0x86, 0x31, 0x8C, 0x01, 0x86, 0x01, 0x00, 0x00, 0x00, 0x00, 0x07, 0x18,
+  0x86, 0x31, 0x9C, 0x01, 0x86, 0x31, 0x8C, 0x01, 0x86, 0x01, 0x00, 0x00,
+  0x00, 0x00, 0x07, 0x18, 0x86, 0x71, 0x0E, 0x63, 0xCE, 0x71, 0x0E, 0x63,
+  0xCE, 0x01, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x19, 0x86, 0xF1, 0x07, 0x7E,
+  0xFC, 0xE1, 0x0F, 0x7E, 0xFC, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
+  0x00, 0x00, 0x00, 0x00
+};
+
+EXPORT_SYMBOL(gioxa_embedded_bits);
+
+MODULE_DESCRIPTION("LOGO for GiOXA SH1106 OLED controller");
+MODULE_AUTHOR("Danny Goossen<danny@gioxa.com>");
+MODULE_LICENSE("GPL");
diff --git a/drivers/video/fbdev/gioxa/gioxa_embedded_logo.h b/drivers/video/fbdev/gioxa/gioxa_embedded_logo.h
new file mode 100644
index 0000000..f8aed29
--- /dev/null
+++ b/drivers/video/fbdev/gioxa/gioxa_embedded_logo.h
@@ -0,0 +1,9 @@
+#ifndef __GIOXA_EMBEDDED_H
+#define __GIOXA_EMBEDDED_H
+
+#define GIOXA_EMBEDDED_WIDTH 128
+#define GIOXA_EMBEDDED_HEIGHT 64
+
+extern unsigned char gioxa_embedded_bits[];
+
+#endif /* __GIOXA_EMBEDDED_H */
diff --git a/drivers/video/fbdev/gioxa/sh1106fb.c b/drivers/video/fbdev/gioxa/sh1106fb.c
new file mode 100644
index 0000000..ec34986
--- /dev/null
+++ b/drivers/video/fbdev/gioxa/sh1106fb.c
@@ -0,0 +1,802 @@
+// SPDX-License-Identifier: GPL-2.0-or-later
+/*
+ * Driver for the cheap SH1106 OLED controller
+ *
+ * Based on `Driver for the Solomon SSD1307 OLED controller from Maxime Ripard`
+ * with init & write sequence from
+ *      `FB driver for the SH1106 OLED Controller from Heiner Kallweit`
+ *
+ * Copyright (C) 2021 Danny Goossen @ Gioxa Ltd
+ *
+ * see file: Documentation/devicetree/bindings/display/sh1106fb.txt
+ */
+
+#include <linux/backlight.h>
+#include <linux/delay.h>
+#include <linux/fb.h>
+#include <linux/gpio/consumer.h>
+#include <linux/i2c.h>
+#include <linux/kernel.h>
+#include <linux/module.h>
+#include <linux/of_device.h>
+#include <linux/of_gpio.h>
+#include <linux/uaccess.h>
+#include <linux/regulator/consumer.h>
+#include "gioxa_embedded_logo.h"
+
+#define MIN(x, y) (((x) < (y)) ? (x) : (y))
+
+#define SH1106FB_DATA                   0x40
+#define SH1106FB_COMMAND                0x80
+
+#define SH1106FB_SET_ADDRESS_MODE       0x20
+#define SH1106FB_SET_ADDRESS_MODE_PAGE  0x02
+
+#define SH1106FB_CONTRAST               0x81
+#define SH1106FB_SET_LOOKUP_TABLE       0x91
+
+#define SH1106FB_CHARGE_PUMP            0x8d
+#define SH1106FB_SEG_REMAP_ON           0xa1
+#define SH1106FB_DISPLAY_OFF            0xae
+#define SH1106FB_SET_MULTIPLEX_RATIO    0xa8
+#define SH1106FB_DISPLAY_ON             0xaf
+#define SH1106FB_START_PAGE_ADDRESS     0xb0
+#define SH1106FB_SET_DISPLAY_OFFSET     0xd3
+#define SH1106FB_SET_CLOCK_FREQ         0xd5
+
+#define SH1106FB_SET_PRECHARGE_PERIOD   0xd9
+#define SH1106FB_SET_COM_PINS_CONFIG    0xda
+#define SH1106FB_SET_START_LINE         0x40
+#define SH1106FB_SET_START_COL_LOW      0x00
+#define SH1106FB_SET_START_COL_HIGH     0x10
+
+#define SH1106FB_SET_VCOMH              0xdb
+
+#define MAX_CONTRAST 255
+
+#define REFRESHRATE 2
+
+static u_int refreshrate = REFRESHRATE;
+module_param(refreshrate, uint, 0);
+
+struct sh1106fb_par;
+
+struct sh1106fb_array {
+         u8      type;
+         u8      data[0];
+};
+
+struct sh1106fb_deviceinfo {
+        u32 default_vcomh;
+        u32 default_dclk_div;
+        u32 default_dclk_frq;
+        int need_chargepump;
+        u32 default_col_offset;
+};
+
+struct sh1106fb_par {
+        unsigned area_color_enable : 1;
+        unsigned com_invdir : 1;
+        unsigned com_lrremap : 1;
+        unsigned com_seq : 1;
+        unsigned lookup_table_set : 1;
+        unsigned seg_remap : 1;
+        unsigned init_tx : 1;
+        u32 com_offset;
+        u32 contrast;
+        u32 dclk_div;
+        u32 dclk_frq;
+        const struct sh1106fb_deviceinfo *device_info;
+        struct i2c_client *client;
+        u32 height;
+        struct fb_info *info;
+        u8 lookup_table[4];
+        u32 col_offset;
+        u32 prechargep1;
+        u32 prechargep2;
+        struct gpio_desc *reset;
+        struct regulator *vbat_reg;
+        u32 vcomh;
+        u32 width;
+        struct sh1106fb_array ** tx_pages;
+        struct sh1106fb_array * tx_buffer;
+        u32 npages;
+};
+
+
+
+static const struct fb_fix_screeninfo sh1106fb_fix = {
+        .id             = "Gioxa Ltd SH1106",
+        .type           = FB_TYPE_PACKED_PIXELS,
+        .visual         = FB_VISUAL_MONO10,
+        .xpanstep       = 0,
+        .ypanstep       = 0,
+        .ywrapstep      = 0,
+        .accel          = FB_ACCEL_NONE,
+};
+
+static const struct fb_var_screeninfo sh1106fb_var = {
+        .bits_per_pixel = 1,
+        .red = { .length = 1 },
+        .green = { .length = 1 },
+        .blue = { .length = 1 },
+};
+
+static struct sh1106fb_array *sh1106fb_alloc_array(u32 len)
+{
+        struct sh1106fb_array *array;
+
+        array = kzalloc(sizeof(struct sh1106fb_array) + len, GFP_KERNEL);
+        if (!array)
+                return NULL;
+
+        array->type = SH1106FB_DATA;
+
+        return array;
+}
+
+static struct sh1106fb_array **sh1106fb_alloc_tx_pages(u32 npages, u32 width )
+{
+        int i;
+        struct sh1106fb_array **tx_pages;
+
+        tx_pages =kzalloc(sizeof(void*) * npages, GFP_KERNEL);
+        if (!tx_pages) return NULL;
+
+        for (i=0;i<npages ; i++) {
+                tx_pages[i] = sh1106fb_alloc_array(width);
+                if (!tx_pages[i]) {
+                        int j;
+                        for (j=0;j<i ; j++)
+                                kfree(tx_pages[j]);
+                        kfree(tx_pages);
+                        return NULL;
+                 }
+         }
+         return tx_pages;
+}
+
+void sh1106fb_free_tx_pages(struct sh1106fb_array ** tx_pages,u32 npages)
+{
+        int i;
+        if (tx_pages) {
+                for (i=0;i<npages ; i++) {
+                        if (tx_pages[i])
+                                kfree(tx_pages[i]);
+                }
+                kfree(tx_pages);
+        }
+}
+
+static int sh1106fb_write_array(struct i2c_client *client,
+                                 struct sh1106fb_array *array, u32 len)
+{
+        int ret;
+
+        len += sizeof(struct sh1106fb_array);
+
+        ret = i2c_master_send(client, (u8 *)array, len);
+        if (ret != len) {
+                dev_err(&client->dev, "Couldn't send I2C command.\n");
+                return ret;
+        }
+
+        return 0;
+}
+
+static inline int sh1106fb_write_cmd(struct i2c_client *client, u8 cmd)
+{
+        u8 data[2];
+        int ret;
+        data[0] = SH1106FB_COMMAND;
+        data[1] = cmd;
+        ret = i2c_master_send(client, &data[0], 2);
+        if (ret != 2)
+        {
+                dev_err(&client->dev, "Couldn't send I2C command.\n");
+                return ret;
+        }
+        return 0;
+}
+
+
+static void sh1106fb_update_display(struct sh1106fb_par *par)
+{
+        u8 *vmem = par->info->screen_buffer;
+        unsigned int line_length = par->info->fix.line_length;
+
+        int page, column, line;
+
+        for (page = 0; page < par->npages; page++)
+        {
+                /* set page */
+                sh1106fb_write_cmd(par->client,
+                                        SH1106FB_START_PAGE_ADDRESS | page);
+
+                /* Set Column Address 4 lower bits  */
+                sh1106fb_write_cmd(par->client,
+                        SH1106FB_SET_START_COL_LOW | (par->col_offset & 0x0f));
+                /* Set Column Address 4 higher bits */
+                sh1106fb_write_cmd(par->client,
+                        SH1106FB_SET_START_COL_HIGH | ((par->col_offset & 0xf0)>>4));
+
+                /* loop column */
+                for (column = 0; column < par->width; column++) {
+                        /* number of bits per byte equals page_height */
+                        /*  or the number of lines within the page */
+                        int page_height = 8;
+                        /* clear array data before using this colom*/
+                        par->tx_buffer->data[column] = 0;
+                        /* Last page may be partial */
+                        if (page + 1 == par->npages && par->height % 8)
+                                page_height = par->height % 8;
+                        /* loop lines in page for this column */
+                        for (line = 0; line < page_height; line++) {
+                                u8 byte = vmem[(8 * page + line) *line_length + column / 8];
+                                u8 bit = (byte >> (column % 8)) & 1;
+                                par->tx_buffer->data[column] |= bit << line;
+                        }
+                }
+
+                 /* skip writing to oled if new page is same as saved page */
+                if ( memcmp( par->tx_buffer->data, par->tx_pages[page]->data,
+                                 par->width) !=0 || par->init_tx)
+                {
+                        /* since this page is different, write it to display page */
+                        sh1106fb_write_array( par->client,
+                        par->tx_buffer,
+                        par->width );
+                        /* and update saved page */
+                        memcpy(par->tx_pages[page]->data,
+                                                par->tx_buffer->data,par->width);
+                }
+        }
+}
+
+void load_logo(struct fb_info *info)
+{
+        struct sh1106fb_par *par = info->par;
+        unsigned long logo_size;
+        unsigned long mem_size;
+
+        logo_size = GIOXA_EMBEDDED_WIDTH * GIOXA_EMBEDDED_HEIGHT
+                                        / sizeof(gioxa_embedded_bits[0]);
+        mem_size = info->fix.smem_len;
+
+        /* for now take the smallest buffer to avoid problems */
+        memcpy(info->screen_buffer, gioxa_embedded_bits,
+                                        MIN(mem_size,logo_size));
+        par->init_tx=1;
+        sh1106fb_update_display(par);
+        par->init_tx=0;
+}
+
+static ssize_t sh1106fb_write(struct fb_info *info, const char __user*buf,
+                                                 size_t count, loff_t *ppos)
+{
+        struct sh1106fb_par *par = info->par;
+        unsigned long total_size;
+        unsigned long p = *ppos;
+        void *dst;
+
+        total_size = info->fix.smem_len;
+
+        if (p > total_size)
+                return -EINVAL;
+
+        if (count + p > total_size)
+                count = total_size - p;
+
+        if (!count)
+                return -EINVAL;
+
+        dst = info->screen_buffer + p;
+
+        if (copy_from_user(dst, buf, count))
+                return -EFAULT;
+
+        sh1106fb_update_display(par);
+
+        *ppos += count;
+
+        return count;
+}
+
+static int sh1106fb_blank(int blank_mode, struct fb_info *info)
+{
+        struct sh1106fb_par *par = info->par;
+
+        if (blank_mode != FB_BLANK_UNBLANK)
+                return sh1106fb_write_cmd(par->client,SH1106FB_DISPLAY_OFF);
+        else
+                return sh1106fb_write_cmd(par->client,SH1106FB_DISPLAY_ON);
+}
+
+static void sh1106fb_fillrect(struct fb_info *info,
+                                        const struct fb_fillrect *rect)
+{
+        struct sh1106fb_par *par = info->par;
+        sys_fillrect(info, rect);
+        sh1106fb_update_display(par);
+}
+
+static void sh1106fb_copyarea(struct fb_info *info,
+                                        const struct fb_copyarea *area)
+{
+        struct sh1106fb_par *par = info->par;
+        sys_copyarea(info, area);
+        sh1106fb_update_display(par);
+}
+
+static void sh1106fb_imageblit(struct fb_info *info,
+                                                const struct fb_image *image)
+{
+        struct sh1106fb_par *par = info->par;
+        sys_imageblit(info, image);
+        sh1106fb_update_display(par);
+}
+
+static struct fb_ops sh1106fb_ops = {
+        .owner          = THIS_MODULE,
+        .fb_read        = fb_sys_read,
+        .fb_write       = sh1106fb_write,
+        .fb_blank       = sh1106fb_blank,
+        .fb_fillrect    = sh1106fb_fillrect,
+        .fb_copyarea    = sh1106fb_copyarea,
+        .fb_imageblit   = sh1106fb_imageblit,
+};
+
+static void sh1106fb_deferred_io(struct fb_info *info,
+                                struct list_head *pagelist)
+{
+        sh1106fb_update_display(info->par);
+}
+
+static int sh1106fb_init(struct sh1106fb_par *par)
+{
+        int ret;
+        u32 precharge, dclk, com_invdir, compins;
+        /* TURN DISPLAY OFF */
+        ret = sh1106fb_write_cmd(par->client, SH1106FB_DISPLAY_OFF);
+        if (ret < 0)
+                return ret;
+
+        /* Set initial contrast  OK SH1106*/
+        ret = sh1106fb_write_cmd(par->client, SH1106FB_CONTRAST);
+        if (ret < 0)
+                return ret;
+
+        ret = sh1106fb_write_cmd(par->client, par->contrast);
+        if (ret < 0)
+                return ret;
+
+        /* Set segment re-map OK  (need for SH1106)*/
+        if (par->seg_remap) {
+                ret = sh1106fb_write_cmd(par->client, SH1106FB_SEG_REMAP_ON);
+                if (ret < 0)
+                        return ret;
+        }
+
+        /* Set COM direction OK SH1106*/
+        /* Set COM Output Scan Direction need, set in tree */
+        /* remapped mode. Scan from COM[N-1] to COM0 */
+
+        com_invdir = 0xc0 | par->com_invdir << 3;
+        ret = sh1106fb_write_cmd(par->client,  com_invdir);
+        if (ret < 0)
+                return ret;
+
+        /* Set multiplex ratio value OK SH1106*/
+        ret = sh1106fb_write_cmd(par->client, SH1106FB_SET_MULTIPLEX_RATIO);
+        if (ret < 0)
+                return ret;
+
+        ret = sh1106fb_write_cmd(par->client, par->height - 1);
+        if (ret < 0)
+                return ret;
+
+        /* set display offset value SH1106 OK, defaults to 0 */
+        ret = sh1106fb_write_cmd(par->client, SH1106FB_SET_DISPLAY_OFFSET);
+        if (ret < 0)
+                return ret;
+
+        ret = sh1106fb_write_cmd(par->client, par->com_offset);
+        if (ret < 0)
+                return ret;
+
+        /* Set Display Start Line */
+        ret = sh1106fb_write_cmd(par->client, SH1106FB_SET_START_LINE | 0x0);
+        if (ret < 0)
+                return ret;
+
+        /* Set clock frequency and divider OK SH1106 should be 0x80 */
+        ret = sh1106fb_write_cmd(par->client, SH1106FB_SET_CLOCK_FREQ);
+        if (ret < 0)
+                return ret;
+
+        dclk = ((par->dclk_div - 1) & 0xf) | (par->dclk_frq & 0xf) << 4;
+        ret = sh1106fb_write_cmd(par->client, dclk);
+        if (ret < 0)
+                return ret;
+
+        /* Set precharge period in number of ticks from the internal clock */
+        ret = sh1106fb_write_cmd(par->client, SH1106FB_SET_PRECHARGE_PERIOD);
+        if (ret < 0)
+                return ret;
+
+        //precharge = (par->prechargep1 & 0xf) | (par->prechargep2 & 0xf) << 4;
+        //need 0xf1 for sh1106, check formula
+        // from FB driver for the SH1106 OLED Controller Heiner Kallweit
+        precharge = 0xf1;
+        ret = sh1106fb_write_cmd(par->client, precharge);
+        if (ret < 0)
+                return ret;
+
+
+
+        /* Calc COM Pins Hardware Configuration */
+        if (par->info->var.yres == 64)
+                /* A[4]=1b, Alternative COM pin configuration */
+                compins =0x12;
+        else if (par->info->var.yres == 48)
+                /* A[4]=1b, Alternative COM pin configuration */
+                compins =0x12;
+        else
+                /* A[4]=0b, Sequential COM pin configuration */
+                compins = 0x02;
+
+        /* Set COM pins configuration OK SSH1106*/
+        ret = sh1106fb_write_cmd(par->client, SH1106FB_SET_COM_PINS_CONFIG);
+        if (ret < 0)
+                return ret;
+
+        ret = sh1106fb_write_cmd(par->client, compins);
+        if (ret < 0)
+                return ret;
+
+        /* Set VCOMH OK SH1106 */
+        ret = sh1106fb_write_cmd(par->client, SH1106FB_SET_VCOMH);
+        if (ret < 0)
+                return ret;
+
+        ret = sh1106fb_write_cmd(par->client, par->vcomh);
+        if (ret < 0)
+                return ret;
+
+
+        /* Set lookup table */
+        if (par->lookup_table_set) {
+                int i;
+
+                ret = sh1106fb_write_cmd(par->client,
+                                          SH1106FB_SET_LOOKUP_TABLE);
+                if (ret < 0)
+                        return ret;
+
+                for (i = 0; i < ARRAY_SIZE(par->lookup_table); ++i) {
+                        u8 val = par->lookup_table[i];
+
+                        if (val < 31 || val > 63)
+                                dev_warn(&par->client->dev,
+                "lookup table index %d value out of range 31 <= %d <= 63\n",
+                                                                i, val);
+                        ret = sh1106fb_write_cmd(par->client, val);
+                        if (ret < 0)
+                                return ret;
+                }
+        }
+
+        /* Clear the screen */
+        sh1106fb_update_display(par);
+
+        /* Turn on the display */
+        ret = sh1106fb_write_cmd(par->client, SH1106FB_DISPLAY_ON);
+        if (ret < 0)
+                return ret;
+
+        return 0;
+}
+
+static int sh1106fb_update_bl(struct backlight_device *bdev)
+{
+        struct sh1106fb_par *par = bl_get_data(bdev);
+        int ret;
+        int brightness = bdev->props.brightness;
+
+        par->contrast = brightness;
+
+        ret = sh1106fb_write_cmd(par->client, SH1106FB_CONTRAST);
+        if (ret < 0)
+                return ret;
+        ret = sh1106fb_write_cmd(par->client, par->contrast);
+        if (ret < 0)
+                return ret;
+        return 0;
+}
+
+static int sh1106fb_get_brightness(struct backlight_device *bdev)
+{
+        struct sh1106fb_par *par = bl_get_data(bdev);
+
+        return par->contrast;
+}
+
+static int sh1106fb_check_fb(struct backlight_device *bdev,
+                                                struct fb_info *info)
+{
+        return (info->bl_dev == bdev);
+}
+
+static const struct backlight_ops sh1106fb_bl_ops = {
+        .options        = BL_CORE_SUSPENDRESUME,
+        .update_status  = sh1106fb_update_bl,
+        .get_brightness = sh1106fb_get_brightness,
+        .check_fb       = sh1106fb_check_fb,
+};
+
+static struct sh1106fb_deviceinfo sh1106fb_sh1106_deviceinfo = {
+        .default_vcomh = 0x40,
+        .default_dclk_div = 1,
+        .default_dclk_frq = 8,
+        .need_chargepump = 1,
+        .default_col_offset=2,
+};
+
+static const struct of_device_id sh1106fb_of_match[] = {
+        {
+                .compatible = "gioxa,sh1106fb-i2c",
+                .data = (void *)&sh1106fb_sh1106_deviceinfo,
+        },
+        {},
+};
+MODULE_DEVICE_TABLE(of, sh1106fb_of_match);
+
+static int sh1106fb_probe(struct i2c_client *client,
+                                        const struct i2c_device_id *id)
+{
+        struct backlight_device *bl;
+        char bl_name[12];
+        struct fb_info *info;
+        struct device_node *node = client->dev.of_node;
+        struct fb_deferred_io *sh1106fb_defio;
+        u32 vmem_size;
+        struct sh1106fb_par *par;
+        void *vmem;
+        int ret;
+
+        if (!node) {
+                dev_err(&client->dev, "No device tree data found!\n");
+                return -EINVAL;
+        }
+
+        info = framebuffer_alloc(sizeof(struct sh1106fb_par), &client->dev);
+        if (!info)
+                return -ENOMEM;
+
+        par = info->par;
+        par->info = info;
+        par->client = client;
+
+        par->device_info = of_device_get_match_data(&client->dev);
+
+        par->reset = devm_gpiod_get_optional(&client->dev, "reset",
+                                                        GPIOD_OUT_LOW);
+        if (IS_ERR(par->reset)) {
+                dev_err(&client->dev, "failed to get reset gpio: %ld\n",
+                        PTR_ERR(par->reset));
+                ret = PTR_ERR(par->reset);
+                goto fb_alloc_error;
+        }
+
+        par->vbat_reg = devm_regulator_get_optional(&client->dev, "vbat");
+        if (IS_ERR(par->vbat_reg)) {
+                ret = PTR_ERR(par->vbat_reg);
+                if (ret == -ENODEV) {
+                        par->vbat_reg = NULL;
+                } else {
+                        dev_err(&client->dev,
+                                "failed to get VBAT regulator: %d\n", ret);
+                        goto fb_alloc_error;
+                }
+        }
+
+        if (of_property_read_u32(node, "gioxa,width", &par->width))
+                par->width = 128;
+        if (of_property_read_u32(node, "gioxa,height", &par->height))
+                par->height = 64;
+        if (of_property_read_u32(node, "gioxa,col-offset", &par->col_offset))
+                par->col_offset = par->device_info->default_col_offset;
+        if (of_property_read_u32(node, "gioxa,com-offset", &par->com_offset))
+                par->com_offset = 0;
+        if (of_property_read_u32(node, "gioxa,prechargep1", &par->prechargep1))
+                par->prechargep1 = 2;
+        if (of_property_read_u32(node, "gioxa,prechargep2", &par->prechargep2))
+                par->prechargep2 = 2;
+        if (!of_property_read_u8_array(node, "gioxa,lookup-table",
+                        par->lookup_table, ARRAY_SIZE(par->lookup_table)))
+                par->lookup_table_set = 1;
+
+        par->seg_remap = !of_property_read_bool(node, "gioxa,segment-no-remap");
+        par->com_seq = of_property_read_bool(node, "gioxa,com-seq");
+        par->com_lrremap = of_property_read_bool(node, "gioxa,com-lrremap");
+        par->com_invdir = of_property_read_bool(node, "gioxa,com-invdir");
+
+        par->contrast = 127;
+        par->vcomh = par->device_info->default_vcomh;
+
+        /* Setup display timing */
+        if (of_property_read_u32(node, "gioxa,dclk-div", &par->dclk_div))
+                par->dclk_div = par->device_info->default_dclk_div;
+        if (of_property_read_u32(node, "gioxa,dclk-frq", &par->dclk_frq))
+                par->dclk_frq = par->device_info->default_dclk_frq;
+
+        vmem_size = DIV_ROUND_UP(par->width, 8) * par->height;
+
+        vmem = (void *)__get_free_pages(GFP_KERNEL | __GFP_ZERO,
+                                        get_order(vmem_size));
+        if (!vmem) {
+                dev_err(&client->dev, "Couldn't allocate graphical memory.\n");
+                ret = -ENOMEM;
+                goto fb_alloc_error;
+        }
+
+        sh1106fb_defio = devm_kzalloc(&client->dev, sizeof(*sh1106fb_defio),
+                                       GFP_KERNEL);
+        if (!sh1106fb_defio) {
+                dev_err(&client->dev, "Couldn't allocate deferred io.\n");
+                ret = -ENOMEM;
+                goto fb_alloc_error;
+        }
+
+        sh1106fb_defio->delay = HZ / refreshrate;
+        sh1106fb_defio->deferred_io = sh1106fb_deferred_io;
+
+        info->fbops = &sh1106fb_ops;
+        info->fix = sh1106fb_fix;
+        info->fix.line_length = DIV_ROUND_UP(par->width, 8);
+        info->fbdefio = sh1106fb_defio;
+
+        info->var = sh1106fb_var;
+        info->var.xres = par->width;
+        info->var.xres_virtual = par->width;
+        info->var.yres = par->height;
+        info->var.yres_virtual = par->height;
+
+        info->screen_buffer = vmem;
+        info->fix.smem_start = __pa(vmem);
+        info->fix.smem_len = vmem_size;
+
+        par->npages=DIV_ROUND_UP(par->height, 8);
+        par->tx_pages =sh1106fb_alloc_tx_pages(par->npages, par->width );
+        if (!par->tx_pages )
+        {
+                dev_err(&client->dev, "Couldn't allocate tx_pages.\n");
+                ret = -ENOMEM;
+                goto fb_alloc_tx_pages_error;
+        }
+
+        par->tx_buffer = sh1106fb_alloc_array(par->width);
+        if (!par->tx_buffer )
+         {
+                dev_err(&client->dev, "Couldn't allocate tx_buffer.\n");
+                ret = -ENOMEM;
+                goto fb_alloc_tx_buff_error;
+         }
+         fb_deferred_io_init(info);
+         i2c_set_clientdata(client, info);
+         if (par->reset) {
+                /* Reset the screen */
+                gpiod_set_value_cansleep(par->reset, 1);
+                udelay(4);
+                gpiod_set_value_cansleep(par->reset, 0);
+                udelay(4);
+         }
+
+        if (par->vbat_reg) {
+                ret = regulator_enable(par->vbat_reg);
+                if (ret) {
+                        dev_err(&client->dev,
+                                        "failed to enable VBAT: %d\n",ret);
+                        goto reset_oled_error;
+                }
+        }
+        dev_info(&client->dev,
+                        "/dev/fb%d: %s INIT\n", info->node, info->fix.id);
+
+        ret = sh1106fb_init(par);
+        if (ret)
+                goto regulator_enable_error;
+
+        dev_info(&client->dev,
+                 "/dev/fb%d: %s Load Logo\n", info->node, info->fix.id);
+        load_logo(info);
+
+        ret = register_framebuffer(info);
+        if (ret) {
+                dev_err(&client->dev,
+                        "Couldn't register the framebuffer\n");
+                goto panel_init_error;
+        }
+
+        snprintf(bl_name, sizeof(bl_name), "sh1106fb%d", info->node);
+        bl = backlight_device_register(bl_name, &client->dev, par,
+                                &sh1106fb_bl_ops, NULL);
+        if (IS_ERR(bl)) {
+                ret = PTR_ERR(bl);
+                dev_err(&client->dev,
+                        "unable to register backlight device: %d\n", ret);
+                goto bl_init_error;
+        }
+
+        bl->props.brightness = par->contrast;
+        bl->props.max_brightness = MAX_CONTRAST;
+        info->bl_dev = bl;
+
+        dev_info(&client->dev,
+                "/dev/fb%d: %s framebuffer device registered," \
+                        " using %d bytes of video memory\n",
+                        info->node, info->fix.id, vmem_size);
+        return 0;
+
+bl_init_error:
+        unregister_framebuffer(info);
+panel_init_error:
+regulator_enable_error:
+        if (par->vbat_reg)
+                 regulator_disable(par->vbat_reg);
+reset_oled_error:
+        if (par->tx_buffer)
+                kfree(par->tx_buffer);
+
+fb_alloc_tx_buff_error:
+        sh1106fb_free_tx_pages( par->tx_pages,par->npages);
+
+fb_alloc_tx_pages_error:
+        fb_deferred_io_cleanup(info);
+
+fb_alloc_error:
+        framebuffer_release(info);
+
+        return ret;
+}
+
+static int sh1106fb_remove(struct i2c_client *client)
+{
+        struct fb_info *info = i2c_get_clientdata(client);
+        struct sh1106fb_par *par = info->par;
+
+        sh1106fb_write_cmd(par->client, SH1106FB_DISPLAY_OFF);
+        backlight_device_unregister(info->bl_dev);
+        unregister_framebuffer(info);
+        fb_deferred_io_cleanup(info);
+        sh1106fb_free_tx_pages( par->tx_pages,par->npages);
+        if (par->tx_buffer) kfree(par->tx_buffer);
+        __free_pages(__va(info->fix.smem_start),
+                                        get_order(info->fix.smem_len));
+        framebuffer_release(info);
+
+        return 0;
+}
+
+static const struct i2c_device_id sh1106fb_i2c_id[] = {
+        { "sh1106fb", 0 },
+        { }
+};
+
+MODULE_DEVICE_TABLE(i2c, sh1106fb_i2c_id);
+
+static struct i2c_driver sh1106fb_driver = {
+        .probe = sh1106fb_probe,
+        .remove = sh1106fb_remove,
+        .id_table = sh1106fb_i2c_id,
+        .driver = {
+                .name = "sh1106fb",
+                .of_match_table = sh1106fb_of_match,
+        },
+};
+
+module_i2c_driver(sh1106fb_driver);
+
+MODULE_DESCRIPTION("GIOXA FB driver for the SH1106 OLED controller");
+MODULE_AUTHOR("Danny Goossen<danny@gioxa.com>");
+MODULE_LICENSE("GPL");
